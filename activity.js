function oddEvenChecker(num1){
	if(isNaN(num1)){
		alert("Invalid Input.")
	}
	else{
		if(num1 % 2 == 0){
			console.log("The number is even")
		}
		else if(num1 % 2 != 0){
			console.log("The number is odd")
		}
	}
}

function budgetChecker(num2){
	if(isNaN(num2)){
		alert("Invalid Input.")
	}
	else{
		if(num2 > 40000){
			console.log("You are over the budget")
		}
		else if(num2 < 40000){
			console.log("You have resouces left")
		}
	}
}